FROM ubuntu:14.04

MAINTAINER hurzelchen@googlemail.com

#
# install all needed packages non-interactively
#

RUN apt-get update && \
    export DEBIAN_FRONTEND=noninteractive && \
    apt-get install -y git nano osm2pgsql postgresql postgresql-9.3-postgis-2.1 postgresql-contrib python python-cairo python-gdal python-gtk2 python-mapnik2 python-psycopg2 python-shapely software-properties-common subversion ttf-unifont unzip wget

#
# make all static changes to the database
#

USER postgres

RUN service postgresql start && \
    psql --command "CREATE USER maposmatic WITH SUPERUSER PASSWORD 'maposmatic';" && \
    createdb --template=template0 -E UTF8 -O maposmatic maposmatic && \
    psql -f /usr/share/postgresql/9.3/contrib/postgis-2.1/postgis.sql -d maposmatic && \
    psql -f /usr/share/postgresql/9.3/contrib/postgis-2.1/postgis_comments.sql -d maposmatic && \
    psql -f /usr/share/postgresql/9.3/contrib/postgis-2.1/spatial_ref_sys.sql -d maposmatic && \
    echo "ALTER TABLE geometry_columns OWNER TO maposmatic; ALTER TABLE spatial_ref_sys OWNER TO maposmatic;" | psql -d maposmatic && \
    echo "CREATE EXTENSION hstore;" | psql -d maposmatic

USER root

#
# download and extract upper bavaria
#

RUN cd /root && \
    wget -nv http://download.geofabrik.de/europe/germany/bayern/oberbayern-latest.osm.bz2 && \
    bzip2 -d oberbayern-latest.osm.bz2

#
# run the osm pgsql exporter
#

RUN service postgresql start && \
    export PGPASS=maposmatic && \
    osm2pgsql -s -c -d maposmatic -m -U maposmatic -H localhost -k /root/oberbayern-latest.osm

#
# install mapnik-osm
#

RUN cd /root && \
    svn co http://svn.openstreetmap.org/applications/rendering/mapnik mapnik2-osm && \
    cd /root/mapnik2-osm && \
    sh ./get-coastlines.sh && \
    python ./generate_xml.py --dbname maposmatic --host 'localhost' --user maposmatic --port 5432 --password 'maposmatic'

#
# install ocitysmap
#

RUN cd /root && \
    git clone git://git.savannah.nongnu.org/maposmatic/ocitysmap.git

#
# clone our repository and run the update and create script
#

RUN cd /root && \
    git clone https://hurzelchen@bitbucket.org/hurzelchen/rescuemap.git

#RUN cd /root/rescuemap && \
#    ./updatedb    

